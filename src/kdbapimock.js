var kdbapi = {}


const sleep = (milliseconds) => {
  if (milliseconds == undefined) {
    milliseconds = Math.floor(Math.random() * 1000);
  }
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

var items = {}
items["Welcome"] = `... to the Knowledge Database (KDB).

If you wonder why the layout of this page looks like the early days of the internet and why the content is not formatted: [[I need support]].

KDB is yet another wiki software :-) which stores the pages into plain [[Markdown]] files. In addition, it has special [[Link Syntax]] to structure dependencies between pages.

KDB consist of a two parts:
* The python-based[backend](https://codeberg.org/avanc/kdb-server) which serves the pages via a [[REST-API]] and stores them onto the filesystem.
* The Javascript-based [frontend](https://codeberg.org/avanc/kdb_frontend) as user interface.

Although KDB is still beta (or even alpha), I already use it to structure information at work.
`

items["Markdown"] = `# Heading
## Sub-Heading
### Sub-Sub-Heading

# Font Formating
Emphasis, aka italics, with *asterisks* or _underscores_.
Strong emphasis, aka bold, with **asterisks** or __underscores__.
Combined emphasis with **asterisks and _underscores_**.
Strikethrough uses two tildes. ~~Scratch this.~~

# Lists
## Unordered
* Level 1
  * Level 2
## Ordered
1. One
2. Two

# Links
[inline-style link](https://codeberg.org)

Plain link: https://codeberg.org

[reference-style link][my reference]

[my reference]

Some text to show that the reference links can follow later.
[my reference]: https://codeberg.org

# Images
![alt text](https://codeberg.org/Codeberg/Design/raw/branch/main/logo/icon/png/codeberg-logo_icon_blue-64x64.png)

# Code

Inline \`code\`

\`\`\`javascript
var s = "JavaScript syntax highlighting";
alert(s);
\`\`\`

# Tables

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

# Blockquotes

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote. 


`

items["MarkdownTest"] = `Text with [[link]] in it.

Text with __bold [[link]]__ in it.

Text again with **bold [[link]]** in it.

Text with _italic [[link]]_ in it.

[[Link|Tag1]] with one tag

[[Link|Tag1|Tag2|Tag3|Tag 4]] with four tags and spaces in tag name


* Item 1
* Item 2 with [[link]]
* Item 3
* Item 4 with [[link]] also
`


items["I need support"] = `As KDB is a hobby project, it should be fun. However, in my case this is not given for web design due to missing CSS skills and lack of creativity.

Therefore, I'm searching support for several open points related to frontend development: https://codeberg.org/avanc/kdb_frontend/projects/1472

So HTML, CSS and JavaScript knowledge with some degree of creativity is mandatory. Experience with [Vue](https://vuejs.org/) is beneficial, as KDB relies on it for dynamic page rendering.

Development takes place on [Codeberg](https://codeberg.org). It would be possible to create a mirror on GitHub but I don't see any good reason at the moment.

If interested, just drop me a mail (mail@klomp.eu) or reply on any of the mentioned tickets.
`

items["Link Syntax"] = `In addition to the [[Markdown]] linking, KDB supports double-brackets linking to other KDB pages with optional tags. Tags can be used to give context in the backlinks why a page was referenced.
An example is worth 1000 words: [[Recipes]]`

items["Recipes"] = `Although this page does not contain any references to other pages, a structured list of backlinks is created.`

items["Hot Choclate"] = `[[Recipes|Drinks|Breakfast]]`

items["Pizza"] = `[[Recipes|Lunch]]`

items["Scrambeld Eggs"] = `[[Recipes|Breakfast]]`

kdbapi.getIndex = async function() {
  await sleep()
  var index=[]
  for (var name in items) {
    index.push({name: name})
  }
  console.log(index)
  return index
}


kdbapi.readItem = async function(name) {
  await sleep()
  if (!(name in items)) {
    throw "404 Error"
  }
  return { id: name, content: items[name]}
}

kdbapi.writeItem = async function(name, content) {
  await sleep()
  console.log("Saving")
  items[name] = content
}

kdbapi.deleteItem = async function(name) {
  delete items[name]
}

kdbapi.getReferences= async function(name) {

  var tagsearch=String.raw`(?:\|([^\[\]\|]*))?` /* eslint-disable-line */
  var pattern = new RegExp(String.raw`\[\[${name}${tagsearch}${tagsearch}${tagsearch}${tagsearch}\]\]`, "g") /* eslint-disable-line */

  var mylist ={}
    
  for (var prop in items) {
    console.log("Searching in " + prop);
    
     var matches = new Array();
     var match;

     while((match = pattern.exec(items[prop])) !== null){
        console.log(match)
        matches.push(match);
      }

    for (match in matches) {
      var tag=undefined
      for (let i = 1; i < 5; i++) {
        if (matches[match][i] != undefined) {
          tag=matches[match][i]
          if ( !( tag in mylist)) {
            mylist[tag]=[]
          }
          mylist[tag].push(prop)
        }
      }
      if (tag == undefined) {
        tag="no tag"
        if ( !( tag in mylist)) {
          mylist[tag]=[]
        }
        mylist[tag].push(prop)
      }
    }
    
  }
  
  console.log(mylist)
  return mylist
}

kdbapi.search= async function(searchstring) {

  var pattern = new RegExp(".*"+escapeRegExp(searchstring)+".*", "g")
    
  
  var mylist ={}

  for (var prop in items) {

    
    var matches = new Array();
    var match;

    while((match = pattern.exec(items[prop])) !== null){
       matches.push(match);
    }

    for (match in matches) {
      if ( !( prop in mylist)) {
        mylist[prop]=[]
      }
      mylist[prop].push(matches[match][0])
    }
    
  }

  return mylist
}

function escapeRegExp(string) {
  //https://stackoverflow.com/a/6969486
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

export default kdbapi
