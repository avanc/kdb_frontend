import axios from 'axios'
var kdbapi = {}

kdbapi.getIndex = async function() {
  var response = await axios.get('http://localhost:8888/items')
  return response.data.pages;
}

kdbapi.readItem = async function(name) {
  var data = {}
  var result = await axios.get('http://localhost:8888/items/'+name)
  data["id"]=name
  data["content"] = result.data
  
  return data
}

kdbapi.writeItem = async function(name, content) {
  await axios.put('http://localhost:8888/items/'+name, content)
}

kdbapi.deleteItem = async function(name) {
  await axios.delete('http://localhost:8888/items/'+name)
}

kdbapi.getReferences= async function(name) {
  var result = await axios.get('http://localhost:8888/items/'+name+"/references")

  return result.data
}


kdbapi.search= async function(searchstring) {
  var result = await axios.get('http://localhost:8888/search?' + searchstring)

  return result.data

}

export default kdbapi
