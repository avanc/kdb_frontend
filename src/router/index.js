import { createRouter, createWebHashHistory } from 'vue-router'
import Page from '../views/Page.vue'

const routes = [
  {
    path: '/',
    redirect: '/Welcome'
  },
  {
    path: '/:pagename',
    name: 'Page',
    component: Page
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
